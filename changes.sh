#!/bin/bash

# Dump current settings
current_settings=$(mktemp)
dconf dump / > "$current_settings"

# Create a temporary GNOME profile for default settings
default_profile=$(mktemp -d)
dconf reset -f /

# Dump default settings
default_settings=$(mktemp)
dconf dump / > "$default_settings"

# Restore current settings
dconf load / < "$current_settings"

# Compare the current settings with the default settings
non_default_settings=$(diff -u "$default_settings" "$current_settings" | grep -E '^\+[^+]' | sed 's/^+//')

# add newline between sections
non_default_settings=$(echo "$non_default_settings" | sed 's/^\[/\n[/')
echo "$non_default_settings" > settings.txt

# Clean up temporary files
rm "$current_settings" "$default_settings"
rm -rf "$default_profile"

