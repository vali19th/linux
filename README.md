# Linux scripts

Tested on: PopOS 22.04
I use those scripts to automate installations, configs and backups.
You may need to modify some commands for them to work properly.
Any tips and corrections are welcome.

HOW TO USE:

    chmod +x main.sh
    sudo ./main.sh

TODO:
	- 2023-01-06 -> didn't work
		- nvim PlugInstall
		- psql history -> missing dir
		- pg_hba.conf edit -> new version -> get the version automatically to fix this forever
		- bash themes -> must create a profile before running that snippet
		- super+Q for launching terminal doesn't work
		- can't hide desktop icons
		- super + N shortcuts not removed (the open app at pos N in dock ones)
	- apply vimrc to sudo too
	- pick the fastest mirrors in your country before downloading any packages
	- L_shift + R_shift => Toggle Caps Lock
	- firewall config
	- cp -r .configs/$backup_dir/.config /home/$USER/.config (if it doesn't break anything)


Saved for later:
How to create custom command to open files with nvim: gnome-terminal --window --maximize -e "bash -c \"nvim %U\""

auto create conda envs: "_apps" and "_jupyter"
shortcut for suspend: Super+L => systemctl suspend

