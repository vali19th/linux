# TODO
# fix ModuleNotFoundError: No module named pylsp.plugins.rope_rename -> `\touch ~/.local/share/nvim/mason/packages/python-lsp-server/venv/lib/python3.12/site-packages/pylsp/plugins/rope_rename.py`
# sudo apt install luarocks
# npm install -g neovim
# - disable F10 in terminal so it doesn't move the focus anymore
#    - hamburger menu -> preferences -> general -> disable "Enable the menu accelerator key (F10 by default)"
#       - automate this please
# - change all `go` shortcuts to point to $HOME instead of other partitions in bashrc_go and others, then create symlinks inside linux/main.sh, so everything is in one place and I don't have to think about it anymore
#     - delete all VISIBLE home dirs and create symlinks to them if I have them in the other partitions
#         - rename `downloads` to `Downloads`
# <signal>
#    # 1. Install our official public software signing key:
#    wget -O- https://updates.signal.org/desktop/apt/keys.asc | gpg --dearmor > signal-desktop-keyring.gpg
#    cat signal-desktop-keyring.gpg | sudo tee /usr/share/keyrings/signal-desktop-keyring.gpg > /dev/null
#    # 2. Add our repository to your list of repositories:
#    echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/signal-desktop-keyring.gpg] https://updates.signal.org/desktop/apt xenial main' |\
#      sudo tee /etc/apt/sources.list.d/signal-xenial.list
#    # 3. Update your package database and install Signal:
#    sudo apt update && sudo apt install signal-desktop
# </signal>
# - automate these:
#   - time as 24h instead of AM/PM
#   - anaconda (automate the prompt to say yes to everything, especially to "should it activate base by default")
#   - Ollama
#   - make symlink for nvim, `.psqlrc`, .sqliterc
# - npm install -g tree-sitter-cli
# - extensions
#     - Bluetooth Quick Connect
#     - Bring Out Submenu Of Power Off/Logout Button
#     - Clipboard Indicator
#     - Extensions Sync
#     - GSConnect
#     - Night Theme Switcher
#     - Sound Input & Output Device Chooser
# - anaconda3
#     - download from hard-coded link
#     - run the .sh
#     - conda update conda
#     - conda update anaconda
# - generate conda envs for each version: `for i in {10..12}; do cec 3.$i 3.$i; done`
# - ln -s ?????/system/dotfiles/nvim/ $HOME/.config/nvim
# - firewall
# - reduce grub waiting time
#     - /etc/default/grub
#         - GRUB_DEFAULT=3
#         - GRUB_TIMEOUT=3
# - DNS = 8.8.8.8,8.8.4.4
# - change nvidia to compute mode # it uses less battery by not using it for the display
# - set a shortcut 'xkill' which will let you kill an app by clicking on it
#
# Custom shortcuts
# Ctrl+Shift+PLUS -> xdotool key XF86MonBrightnessUp
# Ctrl+Shift+MINUS -> xdotool key XF86MonBrightnessDown
#
# - extensions
# https://extensions.gnome.org/extension/1319/gsconnect/
# https://extensions.gnome.org/extension/1401/bluetooth-quick-connect/
# https://extensions.gnome.org/extension/4491/privacy-settings-menu/
# https://extensions.gnome.org/extension/3933/toggle-night-light/
# https://extensions.gnome.org/extension/906/sound-output-device-chooser/
# https://extensions.gnome.org/extension/1486/extensions-sync/
# https://extensions.gnome.org/extension/779/clipboard-indicator/
# https://extensions.gnome.org/extension/2917/bring-out-submenu-of-power-offlogout-button/
#
# - automate or have as checklist
#     - extension cfg
#     - firefox cfg
#     - gnome tweaks
#     - system settings
#     - firewall cfg
#     - default apps
#     - startup apps
#         - firefox
#         - terminal
#         - music player, but paused
#
# org.gnome.desktop.wm.keybindings switch-to-workspace-1 ['<Super>1']
# org.gnome.desktop.wm.keybindings switch-to-workspace-2 ['<Super>2']
# org.gnome.desktop.wm.keybindings switch-to-workspace-3 ['<Super>3']
# org.gnome.desktop.wm.keybindings switch-to-workspace-4 ['<Super>4']
# org.gnome.desktop.wm.keybindings move-to-workspace-1 ['<Shift><Super>exclam']
# org.gnome.desktop.wm.keybindings move-to-workspace-2 ['<Shift><Super>at']
# org.gnome.desktop.wm.keybindings move-to-workspace-3 ['<Shift><Super>numbersign']
# org.gnome.desktop.wm.keybindings move-to-workspace-4 ['<Shift><Super>dollar']
# org.gnome.desktop.wm.keybindings show-desktop ['<Super>d']
#
# org.gnome.desktop.datetime automatic-timezone true
# org.gnome.shell disabled-extensions ['ding@rastersoft.com']
# org.gnome.system.locale region 'en_GB.UTF-8'
# org.gnome.system.location enabled false
# org.gtk.Settings.FileChooser clock-format '24h'
# gsettings set org.gtk.Settings.FileChooser date-format 'iso'
#
# org.gnome.gedit.plugins active-plugins ['time', 'quickhighlight', 'openlinks', 'sort', 'spell', 'snippets', 'quickopen', 'externaltools', 'modelines', 'docinfo', 'filebrowser']
# org.gnome.gedit.preferences.editor auto-indent true
# org.gnome.gedit.preferences.editor auto-save true
# org.gnome.gedit.preferences.editor auto-save-interval uint32 1
# org.gnome.gedit.preferences.editor scheme 'pop-dark'
# org.gnome.gedit.preferences.editor tabs-size uint32 4
#
#
#
#
#
# [org/gnome/desktop/interface]
# clock-format='24h'
# color-scheme='prefer-dark'
# enable-hot-corners=true
# show-battery-percentage=true
#
# [org/gnome/desktop/peripherals/touchpad]
# edge-scrolling-enabled=true
# two-finger-scrolling-enabled=false
#
# [org/gnome/desktop/privacy]
# old-files-age=uint32 14
# recent-files-max-age=30
# remove-old-temp-files=true
# remove-old-trash-files=true
#
# [org/gnome/desktop/screensaver]
# lock-delay=uint32 60
# [org/gnome/desktop/session]
# idle-delay=uint32 300
# [org/gnome/desktop/sound]
# allow-volume-above-100-percent=true
#
# [org/gnome/settings-daemon/plugins/media-keys]
# custom-keybindings=['/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/', '/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom2/', '/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/']
# home=['<Super>e']
# mic-mute=['<Shift><Super>parenright']
# play=['<Super>a']
# terminal=['<Super>z']
# volume-down=['<Super>minus']
# volume-mute=['<Super>0']
# volume-up=['<Super>equal']
# www=['<Super>w']
# [org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0]
# binding='<Super>F1'
# command='systemctl suspend'
# name='suspend'
# [org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1]
# binding='<Shift><Super>underscore'
# command='xdotool key --clearmodifiers XF86MonBrightnessDown'
# name='brightness -'
# [org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom2]
# binding='<Shift><Super>plus'
# command='xdotool key --clearmodifiers XF86MonBrightnessUp'
# name='brightness +'
# [org/gnome/settings-daemon/plugins/power]
# sleep-inactive-ac-timeout=1800
# sleep-inactive-ac-type='suspend'
# sleep-inactive-battery-timeout=1800
# sleep-inactive-battery-type='suspend'
#
# [org/gnome/shell/extensions/bluetooth-quick-connect]
# bluetooth-auto-power-on=true
# keep-menu-on-toggle=true
# refresh-button-on=true
# show-battery-value-on=true
#
# [org/gnome/shell/extensions/clipboard-indicator]
# cache-size=10024
# history-size=100
# move-item-first=true
# strip-text=true
#
# [org/gnome/shell/extensions/dash-to-dock]
# extend-height=false
# manualhide=true
#
# [org/gnome/shell/extensions/extensions-sync]
# provider='Gitlab'
#
# [org/gnome/shell/extensions/nightthemeswitcher/gtk-variants]
# night='Adwaita-dark'
# [org/gnome/shell/extensions/nightthemeswitcher/time]
# manual-time-source=true
# nightthemeswitcher-ondemand-keybinding=['<Super>t']
# ondemand-button-placement='panel'
# time-source='nightlight'
# [org/gnome/shell/extensions/pop-cosmic]
# show-applications-button=false
# show-workspaces-button=false
# [org/gnome/shell/extensions/pop-shell]
# active-hint=true
# gap-inner=uint32 0
# gap-outer=uint32 0
# hint-color-rgba='rgba(255,107,0,0.459459)'
# tile-by-default=true
# [org/gnome/shell/keybindings]
# toggle-application-view=@as []
# toggle-overview=@as []

main() {
    funcs=(
        backup
        ### filesystem -- I'm not using it at the moment
        pre_cfg
        py
        packages
        backup
        cfg
        backup
    )

    # start logging
    _log=$(date "+%y%m%d_%H%M%S").log
    exec 3>&1 4>&2  # Save current stdout and stderr
    exec > >(tee -a "$_log") 2>&1

    for f in ${funcs[@]}; do
        echo -e "\n\n\n--------------------------------------------------------"
        echo "<$f>"
        $f # call the function
        echo "</$f>"

    done

    echo "Now you have to manually do these:
    - setup all wifi connections
    - setup all bluetooth connections
    - connect to google in firefox and chrome
    - sync firefox and chrome
    - add ssh key to gitlab and github
    "
    # stop logging
    exec >&3 2>&4  # Restore stdout and stderr
    exec 3>&- 4>&-  # Close file descriptors
    wait # prevent the script from hanging
}

backup() {
    backup=backup/$(printf '%(%Y_%m_%d_%T)T\n' -1)
    backup_default=backup/default
    mkdir -p $backup

    cp -r /home/$USER/.config $backup/config
    cp /etc/gnome/defaults.list $backup/default_apps
    dconf dump / > $backup/distro.dconf

    if [[ $# -ge 1 && $1 == 'default' ]]; then
        rm -rf $backup_default
        mkdir -p $backup_default
        cp -r $backup/* $backup_default
    fi
}

filesystem(){
    PARTITION=$(lsblk | grep 'T  0 part' | awk '{print $4 " " $1}' | sort -r | head -n 1 | grep -Eo 'sd[a-z]+[0-9]+'); # echo $PARTITION
    DATA=/media/data

    # automount partition at /media/data
    sudo mkdir -p "$DATA"
    sudo chown -R $USER:$USER "$DATA"
    sudo find "$DATA" -type d -exec chmod 711 {} \;
    sudo find "$DATA" -type f -exec chmod 600 {} \;
    sudo su -c "echo '/dev/$PARTITION $DATA auto exec,nosuid,nodev,nofail,x-gvfs-show,x-gvfs-name=data 0 0' >> /etc/fstab"
    sudo mount /dev/$PARTITION "$DATA"

    to_remove=(
        Desktop
        Documents
        Downloads
        Music
        Pictures
        Public
        Templates
        Videos
    )

    to_link=(
        Desktop
        Downloads
        apps
        media
        personal
        projects
        study
        system
    )

    for i in ${to_remove[@]}; do
        rm -rf "$HOME/$i"
    done

    for i in ${to_link[@]}; do
        ln -s "$DATA/2022-05-17/2022-05-17_lenovo/$i" "$HOME/"
    done

    ln -s "$DATA/system/dotfiles/gitconfig" "$HOME/.gitconfig"
    ln -s "$DATA/system/dotfiles/psqlrc" "$HOME/.psqlrc"
    ln -s "$DATA/system/dotfiles/pyflyby" "$HOME/.pyflyby"
    ln -s "$DATA/system/dotfiles/sqliterc" "$HOME/.sqliterc"
    ln -s "$DATA/system/dotfiles/xbindkeysrc" "$HOME/.xbindkeysrc"
}

pre_cfg(){
    # install webi to simplify later steps
    curl -sS https://webi.sh/webi | sh
    export PATH="$HOME/.local/bin:$PATH"

    # expect is used in automating prompts
    sudo apt install -y expect
}


py(){
    with_apt=(
        python3-dev
        python3-venv
        python3-pip
    )

    with_pip=(
        pip
        wheel
        cython
        setuptools
    )

    for i in ${with_apt[@]}; do
        sudo apt install -y $i
    done

    for i in ${with_pip[@]}; do
        python3 -m pip install --upgrade --user $i
    done

    # restore my custom touch function
    [[ -n "$custom_touch" ]] && eval "$custom_touch"
}

packages(){
    development=(
        build-essential # C compiler + make
        git # Version Control
        docker # containerization
        docker-buildx # new docker builder
    )

    system=(
        curl # download files from the internet
        ffmpeg # codecs
        gnome-tweaks # gnome tweak tool
        libreoffice # office suite
        libnotify-bin # generate notifications from bash
        locate # updatedb
        mmv # move/copy files from different directories and rename them
        rename # rename files with regex
        screen # run terminal in background
        tree # list directories -- use it like this: tree | less
        ubuntu-restricted-extras # codecs
        xautomation # bind mouse keys
        xbindkeys # bind mouse keys
        xdotool # bash macros
        appimagelauncher # create shortcuts for AppImages
        gnome-shell-extension-appindicator gir1.2-appindicator3-0.1  # appindicators
        software-properties-common  # for ubuntu-cleaner
        ubuntu-cleaner # clean temporary files, caches, etc
        progress # show progress of currently running coreutils
    )

    apps=(
        anki # flash cards
        audacious # music player
        calibre # ebook reader
        deluge # torrent
        mpv # video player
        signal-desktop # messenger
    )

    to_install=(
        ${development[@]}
        ${system[@]}
        ${apps[@]}
    )

    # auto-accept microsoft fonts EULA
    echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | sudo debconf-set-selections

    # PPAs
    sudo add-apt-repository ppa:libreoffice/ppa -y
    sudo add-apt-repository ppa:appimagelauncher-team/stable -y
    sudo add-apt-repository ppa:gerardpuig/ppa -y

    # packages
    sudo apt update
    sudo apt full-upgrade -y
    for i in ${to_install[@]}; do
        sudo apt install -y $i
    done
    sudo apt autoremove -y
    sudo apt-get autoclean -y

    # tools
    webi jq
    webi fd
    webi rg
    webi bat

    # NeoVim
    ## plugin dependencies
    install_rust # this step is too complex, so I moved it to a function
    webi node
    webi golang
    ~/.local/opt/node-*/bin/npm install -g tree-sitter-cli
    sudo apt install -y python3-neovim
    sudo apt install -y xclip # fix clipboard

    mkdir ~/bin
    curl -LO ~/bin/nvim https://github.com/neovim/neovim/releases/download/v0.10.4/nvim-linux-x86_64.appimage
    chmod u+x ~/bin/nvim
    # TODO: create ~/.venv using 3.11+
    # TODO: pip install pynvim
    # TODO: pip install pyflyby

    # Signal (messenger)
    wget -O- https://updates.signal.org/desktop/apt/keys.asc | gpg --dearmor > signal-desktop-keyring.gpg
    cat signal-desktop-keyring.gpg | sudo tee -a /usr/share/keyrings/signal-desktop-keyring.gpg > /dev/null
    echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/signal-desktop-keyring.gpg] https://updates.signal.org/desktop/apt xenial main' | sudo tee -a /etc/apt/sources.list.d/signal-xenial.list
    rm signal-desktop-keyring.gpg
    sudo apt update
    sudo apt install -y signal-desktop

    # google chrome
    wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
    sudo dpkg -i google-chrome-stable_current_amd64.deb
    rm google-chrome-stable_current_amd64.deb

    # OCR
    _ocr=~/bin/normcap
    wget https://github.com/dynobo/normcap/releases/download/v0.5.6/NormCap-0.5.6-x86_64.AppImage $_ocr
    chmod u+x $_ocr

    ln -s $(which fdfind) ~/.local/bin/fd
}

install_rust() {
    expect << EOF
    #!/usr/bin/expect

    # Timeout for each command (in seconds)
    set timeout -1

    # Start the webi installation command
    spawn webi rustlang

    # Expect the prompt and send the default option (1 followed by Enter)
    expect "1) Proceed with standard installation (default - just press enter)"
    send "1\r"

    # Wait for the installation to complete
    expect eof
EOF

    source "$HOME/.cargo/env"
}

cfg(){
    DOTFILES=$HOME/system/dotfiles
    BASHRC=$DOTFILES/bashrc_main
    echo -e "[[ -f $BASHRC ]] && source $BASHRC" >> $HOME/.bashrc
    sudo bash -c "echo -e '[[ -f $BASHRC ]] && source $BASHRC >> /root/.bashrc"
    sed -i '/^HISTSIZE=/d;/^HISTFILESIZE=/d' ~/.bashrc
    source $HOME/.bashrc

    # Terminal Theme
    sudo apt install -y gconf2 dconf-cli uuid-runtime
    git clone https://github.com/Gogh-Co/Gogh.git gogh
    export TERMINAL=gnome-terminal
    profile_create "Default"
    cd gogh/installs
    ./gruvbox.sh
    ./gruvbox-dark.sh
    cd ../..
    rm -rf gogh

    # lower swappiness
    echo "vm.swappiness=10" | sudo tee -a /etc/sysctl.conf

    # start xbindkeys -- I need this twice. Idk why
    killall xbindkeys; xbindkeys -f $DOTFILES/xbindkeysrc
    killall xbindkeys; xbindkeys -f $DOTFILES/xbindkeysrc

    # swap escape with caps lock
    gsettings set org.gnome.desktop.input-sources xkb-options "['caps:escape']"

    # permissions
    sudo chmod -R 755 $HOME/.local
    sudo chmod -R 700 $HOME/.ssh
    sudo chmod 711 $HOME/.ssh

    # Neovim
    mkdir -p $HOME/.config/nvim
    sudo chown $USER:$USER -R $HOME/.config/nvim
    sudo chmod 755 -R $HOME/.config/nvim

    # git
    read -p "Set git name: " _git_name
    read -p "Set git mail: " _git_email
    if [[ -n "$_git_name" || -n "$_git_email" ]]; then
        echo "[user]" > ~/.gitconfig.private
        [[ -n "$_git_name" ]] && echo "    name = $_git_name" >> ~/.gitconfig.private
        [[ -n "$_git_email" ]] && echo "    email = $_git_email" >> ~/.gitconfig.private
    fi

    # ssh key
    pc_model=$(cat /sys/class/dmi/id/sys_vendor | awk '{print tolower($0)}')
    sudo hostname "$pc_model"
    ssh-keygen -t ed25519 -C "$(logname)@$(hostname)" -f "$HOME/.ssh/id_ed25519" -N ""

    # use Intel for graphics and NVIDIA only for compute
    sudo system76-power graphics compute

    # automatically sync my notes
    {
        echo "$(crontab -l 2>/dev/null)"
        echo "* * * * * cd ~/brain && /bin/bash ./git_sync_for_obsidian_pc.sh >> ../brain.log 2>&1"
    } | crontab -

    echo "Running updatedb. This might take a while..."
    sudo updatedb
}

profile_create() {
    if [[ $(profile_exists "$profile") -eq 1 ]]; then
        echo "Profile '$profile' already exists."
        return
    fi

    base_key="/org/gnome/terminal/legacy/profiles:"
    name="$1"
    uuid=$(uuidgen)
    existing_profiles=$(dconf read $base_key/list)
    existing_profiles=${existing_profiles:1:-1}

    if [ -z "$existing_profiles" ]; then
        new_profiles="['$uuid']"
    else
        new_profiles="[$existing_profiles, '$uuid']"
    fi

    dconf write $base_key/list "$new_profiles"
    dconf write $base_key/:$uuid/visible-name "'$name'"
    echo "Profile '$name' created with UUID $uuid"
}

profile_exists() {
    base_key="/org/gnome/terminal/legacy/profiles:"
    existing_profiles=$(dconf read $base_key/list)
    existing_profiles=${existing_profiles:1:-1}
    IFS=', ' read -r -a array <<< "$existing_profiles"

    for uuid in "${array[@]}"; do
        uuid=${uuid//\'/}
        name=$(dconf read $base_key/:$uuid/visible-name)
        name=${name//\'/}
        if [[ "$name" == "$1" ]]; then
            return 1
        fi
    done

    return 0
}


set -x
main
set +x
